// TableRow.js
import React, { Component } from "react";

class TableRow extends Component {
  render() {
    return (
      <tr>
        <td>{this.props.obj.Customer}</td>
        <td>{this.props.obj.Quotes}</td>
      </tr>
    );
  }
}

export default TableRow;
