import React from "react";
import "./App.css";
import TableRow from "./TableRow";
import axios from "axios";
import Create from "./create";
import CreateQuote from "./createQuote";
import UpdateCustomer from "./updateCustomer"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { customers: [], quotes: [], error: null, isLoaded: false };
  }

callAPI(){
  let apiURL = 'http://localhost:8080/api/customers'
  let apiURL2 = 'http://localhost:8080/api/quotes'
  fetch(apiURL)
    .then(response=>response.json())
    .then(
      (result)=>{
        console.log(result)
        this.setState({ isLoaded: true, customers: result })
      },
      (error)=>{
        console.log("There was an error")
        //set error
        this.setState({ isLoaded: true, error: error})
      }
    )
    fetch(apiURL2)
    .then(response=>response.json())
    .then(
      (result)=>{
        console.log(result)
        this.setState({ isLoaded: true, quotes: result })
      },
      (error)=>{
        console.log("There was an error")
        //set error
        this.setState({ isLoaded: true, error: error})
      }
    )
  }

  componentDidMount() {
    this.callAPI()
  }

  handleRemoveCustomer = id => () =>{

    console.log("REMOVE" + id)

    let apiDelete = `http://localhost:8080/api/customers/delete/${id}`
    let options = {
      method: 'DELETE',
      headers:{'Content-Type':'application/json'},
    }
    
  fetch(apiDelete,options)
  .then(response=>response.json())
  .then(
    (result)=>{
      console.log(result)
    },(error)=>{
      console.log(error) 
      console.log("error deleting customer")
    }
    )
  }

  handleRemoveQuote= id => () =>{

    console.log("REMOVE" + id)

    let apiDelete = `http://localhost:8080/api/quotes/delete/${id}`
    let options = {
      method: 'DELETE',
      headers:{'Content-Type':'application/json'},
    }
    
  fetch(apiDelete,options)
  .then(response=>response.json())
  .then(
    (result)=>{
      console.log(result)
    },(error)=>{
      console.log(error) 
      console.log("error deleting quote")
    }
    )
  }

  tabRow1() {
    return this.state.customers.map(customer => (
      <tr>
        <b>{customer.firstName} {customer.lastName}</b>-{customer.zipCode}-{customer.date} <button onClick={this.handleRemoveCustomer(customer.id)}> Remove </button>
      </tr>
    ))
  }


  tabRow2() {
   
    return this.state.quotes.map(quote => (
      <tr>
        <b>email</b>-{quote.email} <b>policyNumber</b>-{quote.policyNumber} <b>price</b>-{quote.price} <b>start</b>-{quote.startDate} <b>end</b>-{quote.endDate} <button onClick={this.handleRemoveQuote(quote.id)}> Remove </button>
      </tr>
    ))
  }

  render() {

    if (this.state.error) {
      return (
          <aside>
              <h4>Error: {this.state.error.message}</h4>
          </aside>
      )
    } else if (!this.state.isLoaded) {
      return (
          <aside>
              <h4>Waiting...</h4>
          </aside>
      )
   } else {
      return (
        <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <Link to={"/"} className="navbar-brand">
              List Customers
            </Link>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={"/create"} className="nav-link">
                    Create Customer
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to={"/createQuote"} className="nav-link">
                    Create Quote
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to={"/updateCustomer"} className="nav-link">
                    Update Customer
                  </Link>
                </li>
              </ul>
            </div>
          </nav>{" "}
          <br />
          <Switch>
            <Route exact path="/create" component={Create} />
            <Route path="/index" component={App} />
          </Switch>
          <Switch>
            <Route exact path="/createQuote" component={CreateQuote} />
            <Route path="/index" component={App} />
          </Switch>
          <Switch>
            <Route exact path="/updateCustomer" component={UpdateCustomer} />
            <Route path="/index" component={App} />
          </Switch>
        </div>
        <div className="App">
          <h1>Customers and Quotes</h1>
          <table border="1" >
            <tbody>
            <th>Customers</th>
            {this.tabRow1()}
            
            </tbody>
          </table>
          <table border="1" >
            <tbody>
            <th>Quotes</th>
            {this.tabRow2()}
            </tbody>
          </table>

        </div>
      </Router>
      )
    }
  }
}
export default App;
