import React, { Component } from "react";
import axios from "axios";

export default class Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      city: "",
      zipCode: "",
      date: ""
    }
  }

  handleFirstName = (e)=> {this.setState({firstName: e.target.value})}
  handleLastName = (e) => {this.setState({lastName: e.target.value})}
  handleCity = (e) => {this.setState({city: e.target.value})}
  handleZipCode = (e) => {this.setState({zipCode: e.target.value})}
  handleDate = (e) => {this.setState({date: e.target.value})}

  handleButtonPress = (e) => {
    this.addCustomer(this.state)
  }

  addCustomer(customer) {
    const apiURL = "http://localhost:8080/api/customers/add"
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( customer )
    }

    console.log(options)

    fetch( apiURL, options )
      .then( response => response.json() )
      .then(
        (result) => {
          console.log(result)
        }, 
        (error) => {
          console.log(error)
          console.log("error creating new customer")
          // this.setState({error: "Error adding customer"} )
        }
       )
  }


  render() {
    return (
      <section style={{ marginTop: 10 }}>
        <h3>Add New Customer</h3>
        <section>
          First Name: <input value={this.state.firstName} onChange={this.handleFirstName}/> <br/>
          Last Name: <input value={this.state.lastName} onChange={this.handleLastName}/> <br/>
          City: <input value={this.state.city} onChange={this.handleCity}/> <br/>
          Zip Code: <input value={this.state.zipCode} onChange={this.handleZipCode}/> <br />
          Date Became Customer: <input value={this.state.date} onChange={this.handleDate}/> <br />
          <button onClick={this.handleButtonPress}>Create New Customer</button>

        </section>
      </section>
    );
  }
}
