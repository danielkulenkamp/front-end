import React, { Component } from "react";
import axios from "axios";

export default class UpdateCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
        id: 1,
        firstName: "",
        lastName: "",
        city: "",
        zipCode: "",
        date: ""
    }
  }

  handleId = (e) => {this.setState({id: e.target.value})}
  handleFirstName = (e)=> {this.setState({firstName: e.target.value})}
  handleLastName = (e) => {this.setState({lastName: e.target.value})}
  handleCity = (e) => {this.setState({city: e.target.value})}
  handleZipCode = (e) => {this.setState({zipCode: e.target.value})}
  handleDate = (e) => {this.setState({date: e.target.value})}

  handleSearchButton = (e) => {
      this.getCustomerById(this.state.id)
  }
  handleUpdate = (e) => {
    this.updateCustomer(this.state)
  }

  updateCustomer(customer) {
    const apiURL = "http://localhost:8080/api/customers/update"
    const options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( customer )
    }

    console.log(options)

    fetch( apiURL, options )
      .then( response => response.json() )
      .then(
        (result) => {
          console.log(result)
        }, 
        (error) => {
          console.log(error)
          console.log("error updating customer")
          // this.setState({error: "Error adding customer"} )
        }
       )
  }


  render() {
    return (
      <section style={{ marginTop: 10 }}>
        <h3>Update Customer</h3>
        <section>
            Find Customer By ID: <input type="number" value={this.state.id} onChange={this.handleId} /> 
            <button onClick={this.handleSearchButton}>Find</button>

        </section>
        <section>
          First Name: <input value={this.state.firstName} onChange={this.handleFirstName}/> <br/>
          Last Name: <input value={this.state.lastName} onChange={this.handleLastName}/> <br/>
          City: <input value={this.state.city} onChange={this.handleCity}/> <br/>
          Zip Code: <input value={this.state.zipCode} onChange={this.handleZipCode}/> <br />
          Date Became Customer: <input value={this.state.date} onChange={this.handleDate}/> <br />
          <button onClick={this.handleUpdate}>Update Customer</button>

        </section>
      </section>
    );
  }
  getCustomerById(id) { 
    let apiURL = `http://localhost:8080/api/customers/${id}`

    fetch(apiURL)
        .then(response => response.json())
        .then(
            (result) => {
              console.log(result)
              this.setState({customer: result})
              this.setState({
                  firstName: result.firstName,
                  lastName: result.lastName,
                  city: result.city,
                  zipCode: result.zipCode,
                  date: result.date
              })
            },
            (error) => {
                console.log(`Error -- App.getCustomersById(${id})`)
                this.setState({isLoaded: true, error: `Error getting customers, ID: ${id}`})
            } 
        )


  }

}
