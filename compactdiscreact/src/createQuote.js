import React, { Component } from "react";
import axios from "axios";

export default class CreateQuote extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      endDate: "",
      policyNumber: 0,
      price: 0,
      startDate: "",
      vin: "",
      customerId: 0
    }
  }

  handlePrice = (e)=> {this.setState({price: e.target.value})}
  handleStartDate = (e) => {this.setState({startDate: e.target.value})}
  handleEndDate = (e) => {this.setState({endDate: e.target.value})}
  handleEmail = (e) => {this.setState({email: e.target.value})}
  handleVin = (e) => {this.setState({vin: e.target.value})}
  handleCustomerId = (e) => {this.setState({customerId: e.target.value})}
  handlePolicyNumber = (e) => {this.setState({policyNumber: e.target.value})}

  handleButtonPress = (e) => {
    this.addQuote(this.state)
  }

  addQuote(quote) {
    const apiURL = "http://localhost:8080/api/quotes/add"
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify( quote )
    }

    console.log(JSON.stringify( quote ))
    console.log(options)

    fetch( apiURL, options )
      .then( response => response.json() )
      .then(
        (result) => {
          console.log(result)
        }, 
        (error) => {
          console.log(error)
          console.log("error creating new quote")
          // this.setState({error: "Error adding customer"} )
        }
       )
  }

  render() {
    return (
      <section style={{ marginTop: 10 }}>
        <h3>Add New Quote</h3>
        <section>
          Price: <input value={this.state.price} onChange={this.handlePrice}/> <br/>
          Start Date: <input value={this.state.startDate} onChange={this.handleStartDate}/> <br/>
          End Date: <input value={this.state.endDate} onChange={this.handleEndDate}/> <br/>
          Email: <input value={this.state.email} onChange={this.handleEmail}/> <br />
          VIN: <input value={this.state.vin} onChange={this.handleVin}/> <br />
          Customer ID: <input value={this.state.customerId} onChange={this.handleCustomerId} /> <br />
          Policy Number: <input value={this.state.policyNumber} onChange={this.handlePolicyNumber} /> <br />
          <button onClick={this.handleButtonPress}>Create New Quote</button>

        </section>
      </section>
    );
  }
}
